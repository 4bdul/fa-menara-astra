// HAMBURGLERv2

function togglescroll() {
  $('body').on('touchstart', function(e) {
    if ($('body').hasClass('noscroll')) {
      e.preventDefault();
    }
  });
}

$(document).ready(function() {
  togglescroll()
  $(".icon").click(function() {
    $(this).toggleClass("position","mobnav");
    $(".mobilenav").fadeToggle(500);
    $(".mid-menu").toggleClass("mid-animate");
    $(".top-menu").toggleClass("top-animate");
    $(".bottom-menu").toggleClass("bottom-animate");
     if ($('.mobilenav').hasClass('display')){
            $('.mobilenav').toggleClass('display-block');
        } else {
            $('.mobilenav').removeClass('display');
          }
  });
  $("li").click(function(){
      $(".mid-menu").removeClass("mid-animate");
      $(".top-menu").removeClass("top-animate");
      $(".bottom-menu").removeClass("bottom-animate");
      if ($('.mobilenav').hasClass('display')){
            $('.mobilenav').toggleClass('display-block');
        } else {
            $('.mobilenav').addClass('display');
            $('.icon').addClass('relative');
          }
      });
  togglescroll()
  $(".icon-nav").click(function() {
    $(".mobile-nav").fadeToggle(500);
    $(".mid-menu").toggleClass("mid-animate");
    $(".top-menu").toggleClass("top-animate");
    $(".bottom-menu").toggleClass("bottom-animate");
  });
});
